
const formatDate = (date) => {
    // const d = new Date(date);
    const d = new Date(date);
    const options = {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        timeZone: 'Asia/Jakarta'
    };


    const year = d.getUTCFullYear().toString();
    const month = d.getUTCMonth().toString().padStart(2, '0');
    const day = d.getUTCDate().toString().padStart(2, '0');
    const hours = (Number(d.getUTCHours() + 7)).toString().padStart(2, '0');
    const minutes = d.getUTCMinutes().toString().padStart(2, '0');
    const seconds = d.getUTCSeconds().toString().padStart(2, '0');


    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`

}

export default formatDate;
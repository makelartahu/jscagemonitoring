import sfController from "../controllers/sfController.js";
import express from "express";

import swaggerJsdoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";

const sfRouter = express.Router();
/**
 * @swagger
 * components:
 *   schemas:
 *     Smart Farming:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *           description: The ID of the sensor data.
 *         temperature:
 *           type: number
 *           description: The value of the temperature.
 *         humid:
 *           type: number
 *           description: The value of the humidity in the vicinity.
 *         weight:
 *           type: number
 *           description: The value of food weight on the tray.
 *         created_at:
 *           type: string
 *           format: date-time
 *           description: The timestamp of the sensor data created.
 *         updated_at:
 *           type: string
 *           format: date-time
 *           description: The timestamp of the sensor data modified.
 */

/**
 * @swagger
 * tags:
 *   name: Sensor Data
 *   description: API endpoints for sensor data
 */
/**
 * @swagger
 * /api/sf/data:
 *   get:
 *     summary: Retrieve sensor data
 *     tags: [Sensor Data]
 *     responses:
 *       200:
 *         description: Successful response
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Smart Farming'
 *   post:
 *     summary: Create sensor data
 *     tags: [Sensor Data]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Smart Farming'
 *           example:
 *             temperature: 27
 *             humid: 60
 *             weight: 300
 *     responses:
 *       201:
 *         description: Sensor data created successfully
 *       400:
 *         description: Invalid request body
 */


sfRouter.get("/data", sfController.get);
sfRouter.post("/data", sfController.create);

export default sfRouter;
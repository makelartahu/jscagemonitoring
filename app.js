import dotenv from 'dotenv';
import web from './applications/web.js';

dotenv.config();

const init = async () => {
  try {
    const port = process.env.PORT;
    web.listen(port, '0.0.0.0', () => {
      // eslint-disable-next-line no-console
      console.log(`Server is running successfully ! port = ${port}`);
    });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(error);
  }
};

init();

export default init;
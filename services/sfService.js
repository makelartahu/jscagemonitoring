import prismaClient from "../applications/database.js";
import formatDate from "../utils/index.js";


const createSfDataService = async (request) => {

    return await prismaClient.sf.create({
        data: request
    });
}

const getSfDataService = async () => {

    const data = await prismaClient.sf.findMany(
        { take: 10, orderBy: { createdAt: 'desc' } }
    );


    const result = data.map((item) => {
        const createdAtDate = formatDate(item.createdAt);
        const updatedAtDate = formatDate(item.updatedAt);

        return {
            id: item.id,
            temperature: item.temperature,
            humid: item.humid,
            weight: item.weight,
            createdAt: createdAtDate,
            updatedAt: updatedAtDate
        }

    })

    return result;
}


export default {
    createSfDataService,
    getSfDataService
}


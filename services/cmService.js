import prismaClient from "../applications/database.js";
import formatDate from "../utils/index.js";


const createCmDataService = async (request) => {

    return await prismaClient.cm.create({
        data: request
    });
}

const getCmDataService = async () => {

    const data = await prismaClient.cm.findMany(
        { take: 10, orderBy: { createdAt: 'desc' } }
    );


    const result = data.map((item) => {

        const createdAtDate = formatDate(item.createdAt);
        const updatedAtDate = formatDate(item.updatedAt);

        return {
            id: item.id,
            temperature: item.temperature,
            humid: item.humid,
            gas: item.gas,
            createdAt: createdAtDate,
            updatedAt: updatedAtDate
        }

    })

    return result;
}


export default {
    createCmDataService,
    getCmDataService
}


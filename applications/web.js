import express from 'express';
import indexRouter from '../routes/index.js';
import swaggerUi from 'swagger-ui-express';
import swaggerJsdoc from 'swagger-jsdoc';
import cors from 'cors';

const options = {
    definition: {
      openapi: '3.0.0',
      info: {
        title: 'Sensor Data Api',
        version: '1.0.0',
      },
    },
    apis: ['./routes/*.js'],
  };
  
  const swaggerSpec = swaggerJsdoc(options);
  

const web = express();

web.use(cors());
web.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
web.use(express.json());
web.use('/api', indexRouter);

export default web;
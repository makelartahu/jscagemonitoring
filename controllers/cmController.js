import cmService from "../services/cmService.js";


const create = async (req, res, next) => {
    try {
        const result = await cmService.createCmDataService(req.body);
        return res.status(201).json({

            status: 'success',
            code: 201,
            data: result
        });
    } catch (e) {
        throw e;
    }
    
}


const get = async (req, res, next) => {
    try {
        const result = await cmService.getCmDataService();
        return res.status(201).json({
            status: 'success',
            code: 201,
            data: result
        });
    } catch (e) {
        throw e;
    }
    
}



export default {
    create,
    get
}
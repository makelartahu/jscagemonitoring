/*
  Warnings:

  - You are about to drop the `sensordata` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE `sensordata`;

-- CreateTable
CREATE TABLE `cm` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,
    `temperature` DECIMAL(5, 2) NOT NULL,
    `humid` DECIMAL(5, 2) NOT NULL,
    `gas` DECIMAL(5, 2) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,
    `temperature` DECIMAL(5, 2) NOT NULL,
    `humid` DECIMAL(5, 2) NOT NULL,
    `weight` DECIMAL(5, 2) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
